import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from transformers import AutoTokenizer, AutoModelForSequenceClassification, AdamW
# Create PyTorch datasets
from torch.utils.data import Dataset, DataLoader
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv('category_names.csv',header=0 )
from sklearn.preprocessing import LabelEncoder
le = LabelEncoder()
#df['category_level1'] = le.fit_transform(df['category_level1'])
df['category_level2'] = le.fit_transform(df['category_level2'])
# Split the dataset into training and validation sets
train_df, val_df = train_test_split(df, test_size=0.2, random_state=42)

from transformers import AutoConfig, AutoModelForSequenceClassification

# Download configuration from huggingface.co and cache.
config = AutoConfig.from_pretrained("bert-base-cased")
model = AutoModelForSequenceClassification.from_config(config)

import torch
tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased")
num_labels = len(df['category_level2'].unique())
model = AutoModelForSequenceClassification.from_pretrained("bert-base-uncased", num_labels=num_labels)

# Tokenize the input texts
train_texts = train_df['category_level3'].tolist()
val_texts = val_df['category_level3'].tolist()

train_encodings = tokenizer(train_texts, truncation=True, padding=True)
val_encodings = tokenizer(val_texts, truncation=True, padding=True)

# Convert labels to PyTorch tensors
train_labels = torch.tensor(train_df['category_level2'].tolist())
val_labels = torch.tensor(val_df['category_level2'].tolist())

class CustomDataset(Dataset):
    def __init__(self, encodings, labels):
        self.encodings = encodings
        self.labels = labels

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        return {key: torch.tensor(val[idx]) for key, val in self.encodings.items()}, self.labels[idx]

train_dataset = CustomDataset(train_encodings, train_labels)
val_dataset = CustomDataset(val_encodings, val_labels)

# Define training parameters
num_epochs = 3
learning_rate = 1e-5
batch_size = 8

# Define optimizer
optimizer = AdamW(model.parameters(), lr=learning_rate)

# Create DataLoader
train_dataloader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
val_dataloader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False)

# Training loop
model.train()
for epoch in range(num_epochs):
    for batch in train_dataloader:
        inputs, labels = batch
        optimizer.zero_grad()
        outputs = model(**inputs, labels=labels)
        loss = outputs.loss
        loss.backward()
        optimizer.step()
        
from sklearn.metrics import classification_report

# ...

# Evaluation
model.eval()
predictions = []
true_labels = []
with torch.no_grad():
    for batch in val_dataloader:
        inputs, labels = batch
        outputs = model(**inputs)
        logits = outputs.logits
        predictions.extend(logits.argmax(dim=1).tolist())
        true_labels.extend(labels.tolist())

# Calculate accuracy
accuracy = accuracy_score(true_labels, predictions)
print(f"Validation Accuracy: {accuracy}")

# Print classification report
class_names = [str(i) for i in range(45)]  # Replace with actual class names
print("Classification Report:")
print(classification_report(true_labels, predictions, target_names=class_names))
